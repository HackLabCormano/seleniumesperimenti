# COS'E'
Script per verificare la disponibilità di appuntamenti per visite mediche/esami specialisti con SSN in Regione Lombardia.
In caso negativo cicla circa ogni ora in 3 province.
In caso positivo avvisa via Telegram che esiste disponibilità e si stoppa.

# COME SI USA
 - creare bot Telegram e segnarsi ```chat_id``` e ```token```
 - inserire le variabili in ```config.sample.ini```
 - rinominare ```config.sample.ini``` in ```config.sample```
 - lanciare ```python main.py```

 # TODO
 Leggere i risultati quando vi è disponibilità e riportarne almeno DATA e LUOGO. (Non appena avrò un esito positivo per disponibilità.. :D)
