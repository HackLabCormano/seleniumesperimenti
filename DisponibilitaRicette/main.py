# -*- coding: utf-8 -*-
"""
Created on Sat Jan 14 12:10:02 2023

@author: Mirko
"""
# https://selenium-python.readthedocs.io/
# https://github.com/mozilla/geckodriver/releases
# ma funziona anche con chromium driver

# coding=utf-8
import configparser
import time
from datetime import datetime
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support.ui import Select

## configuration ##
config = configparser.ConfigParser(inline_comment_prefixes = ("#",))
config.read('config.ini')
# DATI PERSONALI
CF=config.get('PERSONAL', 'CF')
CRS=config.get('PERSONAL', 'CRS')
RICETTA=config.get('PERSONAL', 'RICETTA')
controllo=config.getboolean('PERSONAL', 'controllo')
PROVINCE=config.get('PERSONAL', 'province').split(',')

# TOOL
tim=int(config.get('TOOL', 'tim'))
# TELEGRAM PART
chat_id=config.get('TELEGRAM', 'chat_id')
token=config.get('TELEGRAM', 'token')
def send_telegram_message(text,silenced=False):
    url = f"https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&text={text}&disable_notification={silenced}"
    return(print(requests.get(url).json())) # this sends the message

## START ##
send_telegram_message('Iniziata la ricerca..')

# CHROMIUM/CHROME
options = webdriver.ChromeOptions()
options.add_argument("--no-sandbox") # per farlo andare come root
options.add_argument("--headless") # per non visualizzare nulla

## CERCA ##
trovato=False
while trovato==False:
    driver = webdriver.Chrome(options=options)
    send_telegram_message('LOG:nuovo giro',silenced=True)
    print('LOG:nuovo giro', flush=True)
    # SITI
    driver.get("https://prenotasalute.regione.lombardia.it/prenotaonline/") # QUESTO è il sito!!!
 
    # seleziona "gestisci prenotazioni"
    time.sleep(tim)
    WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn.btn-primary[ng-click='homePubblicaCtrl.prenota()']"))).click()
    
    # PAGINA INSERISCI DATI #
    # campi input
    driver.find_element(By.ID, "cf").send_keys(CF)
    time.sleep(tim)
    driver.find_element(By.ID, "crs").send_keys(CRS)
    time.sleep(tim)
    ricetta_field=driver.find_element(By.ID, "codice").send_keys(RICETTA)
    time.sleep(tim)
      
    # click conferma
    WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn.btn-primary.btn-lg.pull-right.btn-add-margin-top-2x"))).click()
    time.sleep(tim+1)

    chiede_controllo = True if len(driver.find_elements(By.XPATH,'//*[@id="completa-dati-ricetta-rossa-modal"]/div/div/label[2]/span[1]')) > 0 else False
    # seleziona CONTROLLO: SI o NO
    if chiede_controllo == True:
        if controllo==False:
            time.sleep(tim)
            WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="completa-dati-ricetta-rossa-modal"]/div/div/label[2]/span[1]'))).click()
            time.sleep(tim)
            WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn.btn-primary[ng-click='prenotaCompletaDatiCtrl.conferma()'"))).click()
        elif controllo==True:
            time.sleep(tim)
            WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="completa-dati-ricetta-rossa-modal"]/div/div/label[1]/span[1]'))).click()
            time.sleep(tim)
            WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn.btn-primary[ng-click='prenotaCompletaDatiCtrl.conferma()'"))).click()
    
    # PAGINA INSERISCI DOVE-QUANDO #
    #PROVINCE=["MILANO CITTA'","MILANO PROVINCIA","MONZA E DELLA BRIANZA"] -- ORA LO PRENDO DA config.ini
    for provincia in PROVINCE:
        select = Select(driver.find_element(By.ID,'provincia'))  
        # select by visible text
        select.select_by_visible_text(provincia)  
    
        # Conferma
        time.sleep(tim)
        WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn.btn-primary.submit.pull-right"))).click()
        time.sleep(tim)
        
        # Risposta
        if len(driver.find_elements(By.CSS_SELECTOR,'.alert.alert-danger')) > 0:
            WebDriverWait(driver,tim).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button.btn-default[ng-click='failureCtrl.close()'"))).click()
            print(f"no disponibilità :( in {provincia}", flush=True)
            time.sleep(tim)
        else:
            message=f"c'è disponibilità in {provincia}! "
            print(f"{message}", flush=True)
            # data e ora dd/mm/YY H:M:S
            dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            print(f"controllato in questo orario: {dt_string}", flush=True) # serve? SPOILER: prob. no.
            send_telegram_message(f"{message}")
            trovato=True
            break
    driver.quit()
    time.sleep(3070+(3.5*datetime.now().second))
