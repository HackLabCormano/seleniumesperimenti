# https://selenium-python.readthedocs.io/
# https://github.com/mozilla/geckodriver/releases
# ma funziona anche con chromium driver

# coding=utf-8
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# FIREFOX
#options = webdriver.FirefoxOptions()
#options.add_argument("--headless")
#driver = webdriver.Firefox(options=options)

# CHROMIUM/CHROME
options = webdriver.ChromeOptions()
#options.add_argument("--no-sandbox") # per farlo andare come root
#options.add_argument("--headless") # per non visualizzare nulla
driver = webdriver.Chrome(options=options)

driver.get("https://shop.vivaticket.com/ita/resale")

#elem=driver.find_element(By.ID, "go")
#print(elem)
#print(elem.text)
#elem.submit()
#driver.quit()

table = driver.find_elements(By.CLASS_NAME, 'form-row')
print(table)
print(len(table))

#for row in table.findElements(By.xpath("./child::*")):
for row in table.find_elements_by_css_selector("*"):
    #cell = row.find_element(By.CLASS_NAME, 'mb-2.col-md-3.col-6')
    print(row)

driver.quit()