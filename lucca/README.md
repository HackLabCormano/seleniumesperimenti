# Lucca Ticket Monitoring System

A basic bot that use selenium for scrape that page: https://shop.vivaticket.com/ita/resale
in search of a LuccaComics's Ticket on sale.
When found a new entry, it send a basic telegram message to a channel.

## Usage

- Create a bot using BotFather and add it to a channel
- Retrive API TOKEN and ChatID, and save it in a .env file like the one in example
- Start the bot:

`python3 main.py`

## Installation

If you have nix installed, just use:

` nix-shell shell.nix ` 

A new shell with all the dependencies installed will spawn.
Otherwise you need to install:

- Requests
- Selenium
- Firefox Selenium driver
