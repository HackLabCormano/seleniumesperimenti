import re
import os
import requests
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from dotenv import load_dotenv

load_dotenv()

options = Options()
options.headless = True

pattern = re.compile("^LUCCA.*") # search for lucca events in general

driver = webdriver.Firefox(options=options)
driver.get("https://shop.vivaticket.com/ita/resale")
table = driver.find_elements(By.CLASS_NAME, 'mb-2.col-md-3.col-6')

TOKEN = os.getenv('API_TOKEN')
CHAT_ID = os.getenv('CHAT_ID')

message = "NEW LUCCA TICKET ON SALE! Visit: https://shop.vivaticket.com/ita/resale"

url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={CHAT_ID}&text={message}"
welcome_url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={CHAT_ID}&text=WELCOME TO LUCCA MONITORING!"
print(requests.get(welcome_url).json())

already_visited = []

try:
    while True:
        for row in table:
            match = pattern.match(row.text)
            if match:
                if (row not in already_visited):
                    already_visited.append(row)
                    print(requests.get(url).json())
                    print("Adding")
                    print(row.text)
        print("Waiting for next request")
        time.sleep(60)
except (KeyboardInterrupt, SystemExit, Exception):
    bye_url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={CHAT_ID}&text=BYE BYE!"
    print(requests.get(bye_url).json())
    driver.close()
