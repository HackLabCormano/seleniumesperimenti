#!/bin/bash

cd $(dirname $0)

if
 ! ping -q -c2 atrent.it >/dev/null
then
 echo NON OK
 python3 ./attiva.py
 sleep 5
 mosquitto_pub -h atrent.it -t hacklabcormano -m "router ri-connesso"
else
 echo raggiunto
 mosquitto_pub -h atrent.it -t hacklabcormano -m "router OK"
fi

pkill chromium-brows
pkill chromedriver