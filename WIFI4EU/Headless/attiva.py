# https://selenium-python.readthedocs.io/
# https://github.com/mozilla/geckodriver/releases
# ma funziona anche con chromium driver

# coding=utf-8
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# FIREFOX
#options = webdriver.FirefoxOptions()
#options.add_argument("--headless")
#driver = webdriver.Firefox(options=options)

# CHROMIUM/CHROME
options = webdriver.ChromeOptions()
options.add_argument("--no-sandbox") # per farlo andare come root
options.add_argument("--headless") # per non visualizzare nulla
driver = webdriver.Chrome(options=options)

driver.get("http://social.fastcon.eu.wifi/login") # QUESTO!!!
#driver.get("http://social.fastcon.eu/socialwifi/EasyConnect.php")
#driver.get("file:///home/atrentini/HacklabCormano/RouterConfig/WIFI4EU/EasyConnect.html")
#driver.get("http://atrent.it") # solo per le prove

elem=driver.find_element(By.ID, "go")
print(elem)
print(elem.text)
elem.submit()

driver.quit()
