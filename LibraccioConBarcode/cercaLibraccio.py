#!/usr/bin/python3

# https://selenium-python.readthedocs.io/
# https://github.com/mozilla/geckodriver/releases
# ma funziona anche con chromium driver

import sys, getopt

# coding=utf-8
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# FIREFOX
#options = webdriver.FirefoxOptions()
#options.add_argument("--headless")
#driver = webdriver.Firefox(options=options)

# CHROMIUM/CHROME
options = webdriver.ChromeOptions()
#options.add_argument("--no-sandbox") # per farlo andare come root
options.add_argument("--headless") # per non visualizzare nulla
#options.add_argument("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36")

driver = webdriver.Chrome(options=options)

driver.get("https://www.libraccio.it/acquisto-libri-usati/cerca.aspx")

elem=driver.find_element(By.ID, "ctl00_ctl00_ctl00_C_C_C_tEanList")

#print("elem",elem)

#print("elem text",elem.text)


#elem.send_keys(Keys.TAB)
#elem.click()
#elem.send_keys("9788855261609")  # NO
#elem.send_keys("9780451191144")  # NO

elem.send_keys(sys.argv[1])

#elem.submit()

print(elem.text)

sub=driver.find_element(By.ID, "ctl00_ctl00_ctl00_C_C_C_bSearch")
sub.submit()

risultato=driver.find_element(By.CLASS_NAME, "titolo-usato")
print("titolo:",risultato.text)

risultato=driver.find_element(By.ID, "prezzo_lib")
print("prezzo:",risultato.text)

#driver.quit()
